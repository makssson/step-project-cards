// import api from "./api";
import {submitForm} from "../main.js";

class ModalWindow {
    constructor(parentSelector) {
        this.parent = document.getElementById('root');
        this.form = document.createElement('form');
    }

    render() {
        const modalBackground = document.createElement('div');
        const modal = document.createElement('div');
        const modalContent = document.createElement('div');
        const modalClose = document.createElement('div');
        const inputEmail = document.createElement('input');
        const inputPassword = document.createElement('input');
        const buttonSubmit = document.createElement('button');
        const buttonCancel = document.createElement('button');
        const buttonX = document.createElement('button');
        const modalInput = document.createElement('div');


        this.parent.append(modalBackground);
        modalBackground.appendChild(modal);
        modal.append(modalContent);
        modalContent.append(this.form);
        this.form.append(modalClose);
        modal.prepend(buttonX);
        modalClose.append(modalInput);
        modalInput.append(inputEmail,inputPassword);


        modalBackground.classList.add('modal__background');
        modal.classList.add('modal');
        modalContent.classList.add('modal__content');
        buttonSubmit.classList.add('modal__btn');
        buttonSubmit.style.background = 'greenyellow';
        buttonCancel.style.background = 'grey';
        buttonSubmit.textContent = 'OK'
        buttonCancel.textContent = 'Cancel'
        buttonX.textContent = 'X'
        buttonX.classList.add('btnX')
        buttonCancel.classList.add('modal__btn');
        modalInput.classList.add('modalInput');
        inputPassword.classList.add('inputPassword');




        this.form.append(inputPassword,inputEmail,buttonSubmit,buttonCancel);
        // buttonCancel.addEventListener('click', this.closeModal.bind(this));
        this.form.addEventListener('submit', this.handleModalEnter.bind(this));
        inputPassword.name = 'password';
        inputEmail.name = 'email';
        buttonSubmit.type = 'submit';
        // modal.innerHTML = `
        //                 <div class="modal">
        //                    <div class="modal__content">
        //                     <form action="#">
        //                       <div data-close class="modal__close">
        //                         <button class="modal__btn">X</button>
        //                         <div class="modal__input">
        //                           <input required placeholder="Input your email" type="email">
        //                           <input required placeholder="Input your password" type="password">
        //                         </div>
        //                         <div class="modal__button">
        //                           <button class="modal__btn">OK</button>
        //                           <button class="modal__btn">Cancel</button>
        //                         </div>
        //                       </div>
        //                     </form>
        //                   </div>
        //                 </div>
        // `;
    }
    handleModalEnter(event){
        event.preventDefault()
        const input = this.form.querySelectorAll('input');
        const formData = {};
        input.forEach((item) => {
            formData[item.name] = item.value;
        })
        submitForm(formData)
    }
}


const modal = new ModalWindow()
export default modal;
