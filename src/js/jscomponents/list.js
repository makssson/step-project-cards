import api from "./api.js";

class List {
  init(containerId) {
    const emptyDiv = document.getElementById("empty-list");
    const container = document.getElementById(containerId);
    if (api.getAuthStatus()) {
      container.style.display = "block";
      emptyDiv.style.display = "none";
    } else {
      emptyDiv.style.display = "block";
      container.style.display = "none";
    }
  }

  
}

export default new List();
