// token - 141e1517-bfb2-4065-813b-1b2fc2ef3447
const BASE_URL = "https://ajax.test-danit.com/api/v2/cards";

class Api {
  logIn() {
    const nameInput = document.querySelector(".name-input");
    const passInput = document.querySelector(".password-input");

    if (nameInput.value === "mytest@gmail.com" && passInput.value === "12345") {
      this.email = nameInput.value;
      this.password = passInput.value;
      console.log(this.email);
      console.log(this.password);
    }
  }

  async signIn() {
    const response = await fetch(`${BASE_URL}/login`, {
      method: "POST",
      headers: {
        "Content-type": "application/json",
      },
      body: JSON.stringify({
        email: this.email,
        password: this.password,
      }),
    });
    if (response.status !== 200) {
      alert("Incorrect username or password");
    } else {
      const token = await response.text();
      localStorage.setItem("token", token);
      return token;
    }
  }

  getToken() {
    return localStorage.getItem("token");
  }

  getAuthStatus() {
    return !!this.getToken();
  }

  async getCards() {
    const response = await fetch(`${BASE_URL}`, {
      headers: {
        "Content-type": "aplication/json",
        Authorization: `Bearer ${this.getToken()}`,
      },
    });
    const cards = await response.json();
    this.cards = cards;
  }

  render() {
    const cardsContainer = document.getElementById("card-list");
    this.cardsContainer = cardsContainer;

    console.log(this.cards);
    this.cards.forEach((card) => {
      if (card.doctor === "Кардиолог") {
        this.cardsContainer.insertAdjacentHTML(
          "beforeend",
          `
        <li class="visit-card" data-title="${card.title}" data-description="${card.desription}" data-visit-id="${card.id}" data-hurry="${card.hurry}" data-status="${card.status}">
        <h2 class='card_title'>${card.data}</h2>
        <p class='card-text'>${card.title}</p>
        <div class="more-info" data-info-id="${card.id}">
          <p class='card-text'>${card.desription}</p><p class='card-text'>${card.hurry}</p><p class='card-text' >${card.status}</p><p class='card-text'>${card.age}</p><p class='card-text'>${card.weight}</p><p class='card-text'>${card.pressure}</p><p class='card-text'>${card.illness}</p>
        </div>
      
        <button type='button' data-delete="${card.id}" class="card-delete">Delete</button><button class="card-more" data-edit-id="${card.id}">Edit</button><button class="card-info" data-button-id="${card.id}">Show more</button>
      </li>
        `
        );
      } else if (card.doctor === "Стоматолог") {
        this.cardsContainer.insertAdjacentHTML(
          "beforeend",
          `
        <li class="visit-card" data-title="${card.title}" data-description="${card.desription}" data-visit-id="${card.id}" data-hurry="${card.hurry}" data-status="${card.status}">
          <h2 class='card_title'>${card.data}</h2><p class='card-text'>${card.title}</p>
          <div class="more-info" data-info-id="${card.id}">
          <p class='card-text'>${card.desription}</p><p class='card-text'>${card.hurry}</p><p class='card-text'>${card.status}</p><p class='card-text'>${card.date}</p>
          </div>
          <button type='button' data-delete="${card.id}" class="card-delete" >Delete</button><button class="card-more" data-edit-id="${card.id}">Edit</button><button class="card-info" data-button-id="${card.id}">Show more</button>
          </li>
        `
        );
      } else if (card.doctor === "Терапевт") {
        this.cardsContainer.insertAdjacentHTML(
          "beforeend",
          `
        <li class="visit-card" data-title="${card.title}" data-description="${card.desription}" data-visit-id="${card.id}" data-hurry="${card.hurry}" data-status="${card.status}">
          <h2 class='card_title'>${card.data}</h2><p class='card-text'>${card.title}</p>
          <div class="more-info" data-info-id="${card.id}">
          <p class='card-text'>${card.desription}</p><p class='card-text'>${card.hurry}</p><p class='card-text'>${card.status}</p><p class='card-text'>${card.age}</p>
          </div>
          <button type='button' data-delete="${card.id}" class="card-delete" >Delete</button><button class="card-more" data-edit-id="${card.id}">Edit</button><button class="card-info" data-button-id="${card.id}">Show more</button></li>
        `
        );
      }
    });

  }

  deleteCardFromDataBase(id) {
    fetch(`${BASE_URL}/${id}`, {
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${this.getToken()}`,
      },
    }).then((response) => console.log(response));
  }

  cardDelete() {
    const crosses = document.querySelectorAll(".card-delete");
    crosses.forEach((el) => {
      if (el.textContent === "Delete") {
        el.addEventListener("click", (event) => {
          const postId = event.target.getAttribute("data-delete");
          this.deleteCardFromDataBase(postId);
          this.usedCards.forEach((card) => {
            if (card.getAttribute("data-visit-id") === postId) {
              card.remove();
            }
          });
        });
      }
    });
  }

  filterByNameOrDescription() {
    const mainInput = document.getElementById("search-input");
    const searchButton = document.querySelector(".search-button");
    const usedCards = document.querySelectorAll(".visit-card");
    this.usedCards = usedCards;
    searchButton.addEventListener("click", () => {
      //   console.log(this.cards);

      usedCards.forEach((card) => {
        if (
          mainInput.value === card.getAttribute("data-title") ||
          mainInput.value === card.getAttribute("data-description")
        ) {
          card.style.display = "block";
        } else if (mainInput.value === "") {
          card.style.display = "block";
        } else {
          card.style.display = "none";
        }
      });
    });
  }

  filterByHurry() {
    const filterHurry = document.querySelector(".filter-hurry");
    filterHurry.addEventListener("change", (event) => {
      this.usedCards.forEach((card) => {
        if (event.target.value === card.getAttribute("data-hurry")) {
          card.style.display = "block";
        } else if (event.target.value === "") {
          card.style.display = "block";
        } else {
          card.style.display = "none";
        }
      });
    });
  }

  filterByStatus() {
    const filterStatus = document.querySelector(".filter-status");
    filterStatus.addEventListener("change", (event) => {
      this.usedCards.forEach((card) => {
        if (event.target.value === card.getAttribute("data-status")) {
          card.style.display = "block";
        } else if (event.target.value === "") {
          card.style.display = "block";
        } else {
          card.style.display = "none";
        }
      });
    });
  }
}

export default new Api();
