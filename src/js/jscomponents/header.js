import api from "./api.js";

export function init(headerId, buttonId, handleUserAction, handleGuestAction) {
  const headerEl = document.getElementById(headerId);
  renderButton(buttonId, handleUserAction, handleGuestAction);
}

export function renderButton(buttonId, handleUserAction, handleGuestAction) {
  const btnEl = document.getElementById(buttonId);
  btnEl.removeEventListener('click', handleUserAction);
  btnEl.removeEventListener('click', handleGuestAction)
  if (api.getAuthStatus()) {
    btnEl.innerText = "Create card";
    btnEl.addEventListener("click", handleUserAction);
  } else {
    btnEl.innerText = "Login";
    btnEl.addEventListener("click", handleGuestAction);
  }
}
