import api from "./api.js";

class PostModal {
  createPostModal() {
    const backdrop = document.querySelector(".backdrop");
    const modal = document.querySelector(".edit-modal");
    const infoWrapper = document.createElement("div");
    const inputsWrapperFirst = document.createElement("div");
    const inputsWrapperSecond = document.createElement("div");
    const inputsWrapperThird = document.createElement("div");
    const createButton = document.createElement("button");
    const editButton = document.createElement("button");
    editButton.className = "editButton";
    editButton.innerText = "Редактировать";
    createButton.className = "createButton";
    createButton.innerText = "Создать";

    this.editButton = editButton;
    this.createButton = createButton;
    this.backdrop = backdrop;
    this.inputsWrapperFirst = inputsWrapperFirst;
    this.inputsWrapperSecond = inputsWrapperSecond;
    this.inputsWrapperThird = inputsWrapperThird;

    infoWrapper.className = "infoWrapper";
    inputsWrapperFirst.className = "inputsWrapperFirst";
    inputsWrapperSecond.className = "inputsWrapperSecond";
    inputsWrapperThird.className = "inputsWrapperThird";

    modal.insertAdjacentElement("beforeend", infoWrapper);
    infoWrapper.insertAdjacentHTML(
      "beforeend",
      `
            <select class='doctor-select'>
                <option></option>
                <option class="doctor-option" value="1">Кардиолог</option>
                <option class="doctor-option" value="2">Стоматолог</option>
                <option class="doctor-option" value="3">Терапевт</option>
            </select>
        `
    );
    const doctorSelect = document.querySelector(".doctor-select");
    this.doctorSelect = doctorSelect;
    infoWrapper.insertAdjacentElement("afterend", inputsWrapperFirst);
    modal.insertAdjacentElement("beforeend", createButton);
    modal.insertAdjacentElement("beforeend", editButton);

    inputsWrapperFirst.insertAdjacentHTML(
      "beforeend",
      `
                  <input class="doctor-input" type="text" placeholder="Цель визита"/>
                  <input class="doctor-input input-description" type="text" placeholder="Описание"/>
                  <select name="" id="" class="chosen-hurry">
                    <option></option>
                    <option class="hurry-options" value="Обычная">Обычная</option>
                    <option class="hurry-options" value="Приоритетная">Приоритетная</option>
                    <option class="hurry-options" value="Неотложная">Неотложная</option>
                    </select>
                  <select name="" id="" class="chosen-status">
                    <option></option>
                    <option class="status-options" value="Завершён">Завершён</option>
                    <option class="status-options" value="Ожидается">Ожидается</option>
                  </select>
                  <input class="doctor-input input-data" type="text" placeholder="ФИО"/>
                  <input class="doctor-input input-pressure" type="text" placeholder="Обычное давление"/>
                  <input class="doctor-input input-weight" type="text" placeholder="Индекс массы тела"/>
                  <input class="doctor-input input-illness" type="text" placeholder="Перенесенные заболевания сердечно-сосудистой системы"/>
                  <input class="doctor-input input-age" type="text" placeholder="Возраст"/>
                  <input class="doctor-input input-date" type="text" placeholder="Дата последнего посещения"/>
              `
    );
  }

  showPostModal() {
    this.createButton.style.display = "block";
    this.editButton.style.display = "none";
    const inputDescription = document.querySelector(".input-description");
    this.inputDescription = inputDescription;
    const inputStatus = document.querySelector(".chosen-status");
    this.inputStatus = inputStatus;
    const inputHurry = document.querySelector(".chosen-hurry");
    this.inputHurry = inputHurry;
    const inputPersonData = document.querySelector(".input-data");
    this.inputPersonData = inputPersonData;
    const inputPressure = document.querySelector(".input-pressure");
    this.inputPressure = inputPressure;
    const inputWeight = document.querySelector(".input-weight");
    this.inputWeight = inputWeight;
    const inputIllnes = document.querySelector(".input-illness");
    this.inputIllnes = inputIllnes;
    const inputAge = document.querySelector(".input-age");
    this.inputAge = inputAge;
    const inputDate = document.querySelector(".input-date");
    this.inputDate = inputDate;

    this.doctorSelect.addEventListener("change", () => {
      if (event.target.value === "1") {
        this.inputsWrapperFirst.style.display = "grid";
        inputDate.style.display = "none";
        inputPressure.style.display = "block";
        inputWeight.style.display = "block";
        inputIllnes.style.display = "block";
        inputAge.style.display = "block";
      } else if (event.target.value === "2") {
        this.inputsWrapperFirst.style.display = "grid";
        inputDate.style.display = "block";
        inputPressure.style.display = "none";
        inputWeight.style.display = "none";
        inputIllnes.style.display = "none";
        inputAge.style.display = "none";
      } else if (event.target.value === "3") {
        this.inputsWrapperFirst.style.display = "grid";
        inputAge.style.display = "block";
        inputDate.style.display = "none";
        inputPressure.style.display = "none";
        inputWeight.style.display = "none";
        inputIllnes.style.display = "none";
      }
    });
  }

  closePostModal() {
    if (event.target.className === "hero-button") {
      this.backdrop.classList.remove("is-hidden");
    } else if (event.target.className === "backdrop") {
      this.backdrop.classList.add("is-hidden");
    }
  }

  showEditModal() {
    this.createButton.style.display = "none";
    this.editButton.style.display = "block";
    const editModalButton = document.querySelectorAll(".card-more");
    editModalButton.forEach((el) => {
      el.addEventListener("click", () => {
        this.backdrop.classList.remove("is-hidden");
        this.showPostModal();

        this.createButton.style.display = "none";
        this.editButton.style.display = "block";
        this.editButton.setAttribute(
          "data-editButton-id",
          el.getAttribute("data-edit-id")
        );
      });
    });
  }

  editCard() {
    const allCards = document.querySelectorAll(".visit-card");
    allCards.forEach((card) => {
      if (
        this.editButton.getAttribute("data-editButton-id") ===
          card.getAttribute("data-visit-id") &&
        this.doctorSelect.value === "1"
      ) {
        fetch(
          `https://ajax.test-danit.com/api/v2/cards/${card.getAttribute(
            "data-visit-id"
          )}`,
          {
            method: "PUT",
            headers: {
              "Content-type": "aplication/json",
              Authorization: `Bearer 141e1517-bfb2-4065-813b-1b2fc2ef3447`,
            },
            body: JSON.stringify({
              title: "Визит к кардиологу",
              desription: this.inputDescription.value,
              doctor: "Кардиолог",
              data: this.inputPersonData.value,
              hurry: this.inputHurry.value,
              status: this.inputStatus.value,
              age: this.inputAge.value,
              weight: this.inputWeight.value,
              pressure: this.inputPressure.value,
              illness: this.inputIllnes.value,
            }),
          }
        ).then((response) => response.json());
      } else if (
        this.editButton.getAttribute("data-editButton-id") ===
          card.getAttribute("data-visit-id") &&
        this.doctorSelect.value === "2"
      ) {
        fetch(
          `https://ajax.test-danit.com/api/v2/cards/${card.getAttribute(
            "data-visit-id"
          )}`,
          {
            method: "PUT",
            headers: {
              "Content-type": "aplication/json",
              Authorization: `Bearer 141e1517-bfb2-4065-813b-1b2fc2ef3447`,
            },
            body: JSON.stringify({
              title: "Визит к стоматологу",
              desription: this.inputDescription.value,
              doctor: "Стоматолог",
              data: this.inputPersonData.value,
              hurry: this.inputHurry.value,
              status: this.inputStatus.value,
              date: this.inputDate.value,
            }),
          }
        ).then((response) => response.json());
      } else if (
        this.editButton.getAttribute("data-editButton-id") ===
          card.getAttribute("data-visit-id") &&
        this.doctorSelect.value === "3"
      ) {
        fetch(
          `https://ajax.test-danit.com/api/v2/cards/${card.getAttribute(
            "data-visit-id"
          )}`,
          {
            method: "PUT",
            headers: {
              "Content-type": "aplication/json",
              Authorization: `Bearer 141e1517-bfb2-4065-813b-1b2fc2ef3447`,
            },
            body: JSON.stringify({
              title: "Визит к терапевту",
              desription: this.inputDescription.value,
              doctor: "Терапевт",
              data: this.inputPersonData.value,
              hurry: this.inputHurry.value,
              status: this.inputStatus.value,
              age: this.inputAge.value,
            }),
          }
        ).then((response) => response.json());
      }
      card.remove();
    });
  }

  putEditCard() {
    this.editButton.addEventListener("click", () => {
      this.editCard();
      api.getCards().then(() => {
        api.render();
        api.filterByNameOrDescription();
        api.cardDelete();
        api.filterByHurry();
        api.filterByStatus();
        this.showMoreInfo();
        this.showEditModal();
      });

      this.backdrop.classList.add("is-hidden");
    });
  }

  postCard() {
    const cardsContainer = document.getElementById("card-list");
    this.createButton.addEventListener("click", () => {
      if (this.doctorSelect.value === "1") {
        api.getCards().then(() => {
          api.filterByNameOrDescription();
          api.cardDelete();
          api.filterByHurry();
          api.filterByStatus();
          this.showMoreInfo();
          this.showEditModal();
        });
        fetch(`https://ajax.test-danit.com/api/v2/cards`, {
          method: "POST",
          headers: {
            "Content-type": "aplication/json",
            Authorization: `Bearer 141e1517-bfb2-4065-813b-1b2fc2ef3447`,
          },
          body: JSON.stringify({
            title: "Визит к кардиологу",
            desription: this.inputDescription.value,
            doctor: "Кардиолог",
            data: this.inputPersonData.value,
            hurry: this.inputHurry.value,
            status: this.inputStatus.value,
            age: this.inputAge.value,
            weight: this.inputWeight.value,
            pressure: this.inputPressure.value,
            illness: this.inputIllnes.value,
          }),
        })
          .then((response) => response.json())
          .then((response) =>
            cardsContainer.insertAdjacentHTML(
              "beforeend",
              `
          <li class="visit-card" data-title="${response.title}" data-description="${response.desription}" data-visit-id="${response.id}" data-hurry="${response.hurry}" data-status="${response.status}">
            <h2 class='card_title'>${response.data}</h2>
            <p class='card-text'>${response.title}</p>
            <div class="more-info" data-info-id="${response.id}">
              <p class='card-text'>${response.desription}</p><p class='card-text'>${response.hurry}</p><p class='card-text' >${response.status}</p><p class='card-text'>${response.age}</p><p class='card-text'>${response.weight}</p><p class='card-text'>${response.pressure}</p><p class='card-text'>${response.illness}</p>
            </div>
          
            <button type='button' data-delete="${response.id}" class="card-delete">Delete</button><button class="card-more" data-edit-id="${response.id}">Edit</button><button class="card-info" data-button-id="${response.id}">Show more</button>
          </li>
          `
            )
          );

        if (event.target.className === "createButton") {
          this.backdrop.classList.add("is-hidden");
        }
      } else if (this.doctorSelect.value === "2") {
        api.getCards().then(() => {
          api.filterByNameOrDescription();
          api.cardDelete();
          api.filterByHurry();
          api.filterByStatus();
          this.showMoreInfo();
          this.showEditModal();
        });
        fetch(`https://ajax.test-danit.com/api/v2/cards`, {
          method: "POST",
          headers: {
            "Content-type": "aplication/json",
            Authorization: `Bearer 141e1517-bfb2-4065-813b-1b2fc2ef3447`,
          },
          body: JSON.stringify({
            title: "Визит к стоматологу",
            desription: this.inputDescription.value,
            doctor: "Стоматолог",
            data: this.inputPersonData.value,
            hurry: this.inputHurry.value,
            status: this.inputStatus.value,
            date: this.inputDate.value,
          }),
        })
          .then((response) => response.json())
          .then((response) =>
            cardsContainer.insertAdjacentHTML(
              "beforeend",
              `
          <li class="visit-card" data-title="${response.title}" data-description="${response.desription}" data-visit-id="${response.id}" data-hurry="${response.hurry}" data-status="${response.status}">
          <h2 class='card_title'>${response.data}</h2><p class='card-text'>${response.title}</p>
          <div class="more-info" data-info-id="${response.id}">
          <p class='card-text'>${response.desription}</p><p class='card-text'>${response.hurry}</p><p class='card-text'>${response.status}</p><p class='card-text'>${response.date}</p>
          </div>
          <button type='button' data-delete="${response.id}" class="card-delete" >Delete</button><button class="card-more" data-edit-id="${response.id}">Edit</button><button class="card-info" data-button-id="${response.id}">Show more</button>
          </li>
          `
            )
          );
        if (event.target.className === "createButton") {
          this.backdrop.classList.add("is-hidden");
        }
      } else if (this.doctorSelect.value === "3") {
        api.getCards().then(() => {
          api.filterByNameOrDescription();
          api.cardDelete();
          api.filterByHurry();
          api.filterByStatus();
          this.showMoreInfo();
          this.showEditModal();
        });
        fetch(`https://ajax.test-danit.com/api/v2/cards`, {
          method: "POST",
          headers: {
            "Content-type": "aplication/json",
            Authorization: `Bearer 141e1517-bfb2-4065-813b-1b2fc2ef3447`,
          },
          body: JSON.stringify({
            title: "Визит к терапевту",
            desription: this.inputDescription.value,
            doctor: "Терапевт",
            data: this.inputPersonData.value,
            hurry: this.inputHurry.value,
            status: this.inputStatus.value,
            age: this.inputAge.value,
          }),
        })
          .then((response) => response.json())
          .then((response) =>
            cardsContainer.insertAdjacentHTML(
              "beforeend",
              `
          <li class="visit-card" data-title="${response.title}" data-description="${response.desription}" data-visit-id="${response.id}" data-hurry="${response.hurry}" data-status="${response.status}">
          <h2 class='card_title'>${response.data}</h2><p class='card-text'>${response.title}</p>
          <div class="more-info" data-info-id="${response.id}">
          <p class='card-text'>${response.desription}</p><p class='card-text'>${response.hurry}</p><p class='card-text'>${response.status}</p><p class='card-text'>${response.age}</p>
          </div>
          <button type='button' data-delete="${response.id}" class="card-delete" >Delete</button><button class="card-more" data-edit-id="${response.id}">Edit</button><button class="card-info" data-button-id="${response.id}">Show more</button></li>
          `
            )
          );
        if (event.target.className === "createButton") {
          this.backdrop.classList.add("is-hidden");
        }
      }
    });
  }
  showMoreInfo() {
    const showMoreButton = document.querySelectorAll(".card-info");
    showMoreButton.forEach((el) => {
      el.addEventListener("click", (event) => {
        const allCards = document.querySelectorAll(".visit-card");
        const allInfo = document.querySelectorAll(".more-info");
        allCards.forEach((card) => {
          allInfo.forEach((elem) => {
            if (
              elem.getAttribute("data-info-id") ===
              event.target.getAttribute("data-button-id")
            ) {
              elem.style.display = "block";
            }
          });
        });
      });
    });
  }

  action() {}
}

export default new PostModal();
