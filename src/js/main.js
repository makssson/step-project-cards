import api from "./jscomponents/api.js";
import list from "./jscomponents/list.js";
import postModal from "./jscomponents/post-modal.js";
import { init as initHeader, renderButton } from "./jscomponents/header.js";

const firstModal = document.querySelector(".modal-wrapper");
const firstModalButton = document.querySelector(".first-login");

postModal.createPostModal();


const showModal = () => {
  firstModal.style.display = "flex";
};
const closeModal = () => {
  firstModal.style.display = "none";
};

const handleLogin = () => {
  showModal();
  firstModalButton.addEventListener("click", (e) => {
    e.preventDefault();
    api.logIn();
    api.signIn().then(() => {
      console.log(api.getToken());
      renderButton("action-button", handleCreateCard, handleLogin);
      list.init("container");
      api.getCards().then(() => {
        api.render();
        api.filterByNameOrDescription();
        api.cardDelete();
        api.filterByHurry();
        api.filterByStatus();
        postModal.postCard();
        postModal.showMoreInfo();
        postModal.showEditModal();
        postModal.putEditCard()
      });
    });
    closeModal();
  });
};

if (api.getToken()) {
  api.getCards().then(() => {
    api.render();
    api.filterByNameOrDescription();
    api.cardDelete();
    api.filterByHurry();
    api.filterByStatus();
    postModal.showMoreInfo();
    postModal.showEditModal();
    postModal.postCard();
    postModal.putEditCard()
  });
}

const handleCreateCard = () => {
  console.log("Create card");
  postModal.showPostModal();
  postModal.closePostModal();
  api.getCards().then(() => {
    api.filterByNameOrDescription();
    api.cardDelete();
    api.filterByHurry();
    api.filterByStatus();
    postModal.showMoreInfo();
  });
};

initHeader("header", "action-button", handleCreateCard, handleLogin);
list.init("container");


const backdrop = document.querySelector(".backdrop");
const listContainer = document.querySelector(".card-list");
const clsModal = document.querySelector(".close-modal_button");

clsModal.addEventListener("click", closeEditModal);
window.addEventListener("click", (e) => {
  if (e.target.className === "backdrop") {
    closeEditModal();
  }
});


function closeEditModal() {
  backdrop.classList.add("is-hidden");
}

